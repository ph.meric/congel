<?php

namespace App\Form;

use App\Entity\Category;
use App\Produit\ProduitData;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Nomproduit', TextType::class,['label'=>'Produit : ','attr'=>['autofocus'=>null]])
            ->add('category',EntityType::class,['class'=>Category::class,
                'choice_label'=>'Name',
                'choice_value'=>'id',
                'placeholder'=>'Choisissez une catégorie de produit'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProduitData::class,
        ]);
    }
}
