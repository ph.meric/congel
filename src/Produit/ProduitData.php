<?php


namespace App\Produit;


use App\Entity\Category;
use App\Entity\Produit;
use Symfony\Component\Validator\Constraints as Assert;

class ProduitData
{
    /**
     * @Assert\NotBlank(message="Indiquez le nom de votre produit")
    */
    public string $Nomproduit;
    /**
     * @Assert\NotBlank(message="Indiquez la catégorie")
     */
    public Category $category;

    public function  fromProduitEntity(Produit $produit){
        $this->Nomproduit=$produit->getNomproduit();
        $this->category=$produit->getCategory();

    }


}