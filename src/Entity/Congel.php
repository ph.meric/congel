<?php

namespace App\Entity;

use App\Repository\CongelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=CongelRepository::class)
 */
class Congel
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="uuid")
     */
    private $identificateur;

    /**
     * @ORM\OneToMany(targetEntity=Tiroir::class, mappedBy="congel", orphanRemoval=true,cascade="persist")
     */
    private $tiroirs;

    /**
     * Congel constructor.
     */
    public function __construct()
    {
        $this->tiroirs = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getNom();
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentificateur():Uuid
    {
        return $this->identificateur;
    }

    public function setIdentificateur(Uuid $identificateur): self
    {
        $this->identificateur = $identificateur;

        return $this;
    }

    public function getNbTiroirs(): int
    {
        return $this->getTiroirs()->count();
    }

    /**
     * @return Collection|tiroir[]
     */
    public function getTiroirs(): Collection
    {
        return $this->tiroirs;
    }

    public function addTiroir(tiroir $tiroir): self
    {
        if (!$this->tiroirs->contains($tiroir)) {
            $this->tiroirs[] = $tiroir;
            $tiroir->setCongel($this);
        }

        return $this;
    }

    public function removeTiroir(tiroir $tiroir): self
    {
        if ($this->tiroirs->removeElement($tiroir)) {
            // set the owning side to null (unless already changed)
            if ($tiroir->getCongel() === $this) {
                $tiroir->setCongel(null);
            }
        }

        return $this;
    }
}
