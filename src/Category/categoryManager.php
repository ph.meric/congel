<?php


namespace App\Category;


use App\Entity\Category;
use App\Entity\Congel;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class categoryManager
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * CongelManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function CreateCategory(categoryData $data): void
    {
        $category = new Category();
        $category->setName($data->name);
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }

    public function updateCategory(categoryData $categoryData, Category $category)
    {
        if ($category->getName() == ""){
            throw new Exception('Le nom de la catégorie est obligatoire');}
            $category->setName($categoryData->name);
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }
    Public function DeleteCategory(Category $category){
        $this->entityManager->remove($category);
        $this->entityManager->flush();
    }
}