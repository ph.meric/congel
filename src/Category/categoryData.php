<?php


namespace App\Category;


use App\Entity\Category;
use Symfony\Component\Validator\Constraints as Assert;

class categoryData
{
    /**
     * @Assert\NotBlank(message="Indiquez le nom de la catégorie")
     */
    public string $name;

    public function fromCongelEntity(Category $category): categoryData
    {
        $this->name = $category->getName();
        return $this;
    }

}