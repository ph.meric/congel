<?php


namespace App\Congel;


use App\Entity\Congel;
use App\Entity\Tiroir;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Uid\Uuid;

class CongelManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CongelManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param CongelData $data
     */
    public function CreateCongel(CongelData $data): void
    {
        $congel = new Congel();
        $congel->setNom($data->nom);
        $congel->setIdentificateur(Uuid::v1());
        $nbTiroirs = $data->nbTiroirs;
        for ($i = 1; $i <= $nbTiroirs; $i++) {
            $tiroir = new Tiroir();
            $tiroir->setNom("Tiroir " . $i);
            $tiroir->setOrdre($i);
            $congel->addTiroir($tiroir);
        }
        $this->entityManager->persist($congel);
        $this->entityManager->flush();
    }

    public function UpdateCongel(CongelData $congelData, Congel $congel)
    {
        if ($congelData->nom == ""){
            throw new Exception('Le nom du congélateur est obligatoire');}
        $congel->setNom($congelData->nom);
        $this->entityManager->persist($congel);
        $this->entityManager->flush();
    }
    Public function DeleteCongel(Congel $congel){
        $this->entityManager->remove($congel);
        $this->entityManager->flush();
    }

}