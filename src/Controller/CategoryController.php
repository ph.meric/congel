<?php

namespace App\Controller;

use App\Category\categoryData;
use App\Category\categoryManager;
use App\Congel\CongelManager;
use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category",name="category")
     * @param CategoryRepository $repository
     * @return Response
     */
    public function index(CategoryRepository $repository): Response
    {
        $categories=$repository->findAll();

        return $this->render('category/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/category/add",name="addcategory",methods={"GET","POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function Add(Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(CategoryType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $categoryManager = new categoryManager($em);
            $categoryManager->CreateCategory($form->getData());
            $this->addFlash('success', 'Nouvelle catégorie créée');
            return $this->redirectToRoute('category');
        }
        return $this->render('category/new.html.twig', [
            'category_add' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/edit/{id}",name="editcategory",methods={"GET","POST"})
     */
    public function edit(Category $category, Request $request, EntityManagerInterface $em): Response
    {

        $CategoryDisplay = new categoryData();
        $CategoryDisplay->fromCongelEntity($category);
        $form = $this->createForm(CategoryType::class, $CategoryDisplay);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $CategoryManager = new categoryManager($em);
            $CategoryManager->UpdateCategory($form->getData(), $category);
            $this->addFlash('success', 'Modification enregistrée');
            return $this->redirectToRoute('category');
        }
        return $this->render('category/edit.html.twig', [
            'category_edit' => $form->createView(),
        ]);
    }
    /**
     * @Route("/category/delete/{id}",name="deletecategory",methods={"DELETE"})
     */
    public function delete(Category $category,EntityManagerInterface $entityManager): Response
    {
        $categoryManager = new categoryManager($entityManager);
        $categoryManager-> DeleteCategory($category);
        $this->addFlash('success', 'Suppression effectuée');
        return $this->redirectToRoute('category');
    }
}
