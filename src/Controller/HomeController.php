<?php


namespace App\Controller;


use App\Repository\CongelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class HomeController extends AbstractController
{
    /**
     * @Route("/",name="home")
     * @param CongelRepository $repository
     * @return Response
     */
    public function index(CongelRepository $repository): Response
    {
        $congelateurs = $repository->findAll();
        return $this->render('Pages/home.html.twig', [
            'congelateurs' => $congelateurs,
        ]);
    }


}